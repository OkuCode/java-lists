package com.umanizales.lists_prog2.controller;

import com.umanizales.lists_prog2.controller.dto.ResponseDTO;
import com.umanizales.lists_prog2.exception.ListaDeException;
import com.umanizales.lists_prog2.model.Boy;
import com.umanizales.lists_prog2.model.Location;
import com.umanizales.lists_prog2.service.ListaDeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Class boysde controller, for all the queries to the application with double linked list
 * Here All the petitions goes through postman
 * RestController become a controller for all order on our app
 * Request mapping to make a request to the server
 * Post Mapping to see the response thought postman
 * Autowired: inject dependencies for all operation, but only when it's called*/

@RestController
@RequestMapping(path = "boysde")
public class BoysDeController {
    @Autowired
    private ListaDeService listaDeService;

    @PostMapping
    public ResponseEntity<ResponseDTO> addBoy(@RequestBody @Valid Boy boy) throws ListaDeException{
        return listaDeService.addBoy(boy);
    }

    @GetMapping(path = "count")
    public ResponseEntity<ResponseDTO> count(){
        return listaDeService.Count();
    }

    @PostMapping(path = "addtostart")
    public ResponseEntity<ResponseDTO> addBoyToStart(@RequestBody Boy boy) {
        return listaDeService.addBoyToStart(boy);
    }

    @PostMapping(path = "addbyindex/{index}")
    public ResponseEntity<ResponseDTO> addByIndex(@PathVariable int index, @RequestBody Boy boy){
        return listaDeService.addByIndex(boy, index);
    }

    @GetMapping(path = "invert")
    public ResponseEntity<ResponseDTO> invert(){
        return listaDeService.invert();
    }

    @GetMapping(path = "delete/{identification}")
    public ResponseEntity<ResponseDTO> deleteById(@PathVariable String identification){
        return listaDeService.deleteById(identification);
    }

    @GetMapping(path = "listboysgender/{gender}")
    public ResponseEntity<ResponseDTO> listBoysByGender(@PathVariable String gender) throws ListaDeException {
        return listaDeService.listBoysByGender(gender);
    }

    @GetMapping(path = "reorderbygender")
    public ResponseEntity<ResponseDTO> reorderByGender() throws ListaDeException {
        return listaDeService.reorderByGender();
    }

    @GetMapping(path = "boysbylocation")
    public ResponseEntity<ResponseDTO> boysByLocation(){
        return listaDeService.totalBoysByLocation();
    }

    @GetMapping
    public ResponseEntity<ResponseDTO> listBoysDe() throws ListaDeException{
        return listaDeService.listBoysDe();
    }

}
