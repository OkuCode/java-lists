package com.umanizales.lists_prog2.controller.dto;

import com.umanizales.lists_prog2.model.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class GenderGradesByRHDTO {
    private Gender gender;
    private GradeRHDTO[] gradeRHDTOList;
}


