package com.umanizales.lists_prog2.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class GenderByGradesDTO {
    private byte grade;
    private int total;
    private List<CountByGenderDTO> countByGenderDTOList;
}
