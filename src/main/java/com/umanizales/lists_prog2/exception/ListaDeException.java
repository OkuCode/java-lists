package com.umanizales.lists_prog2.exception;


/** Class listDeException: handle all errors for the
 * double linked lists and the methods used to manipulate this
 * data structure*/

public class ListaDeException extends Exception{
    public ListaDeException(String message) {
        super(message);
    }
}
