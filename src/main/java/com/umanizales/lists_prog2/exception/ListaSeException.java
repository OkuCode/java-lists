package com.umanizales.lists_prog2.exception;

/** Class listSeException: handle all errors for the
 * linked lists and the methods used to manipulate this
 * data structure*/

public class ListaSeException extends Exception{
    public ListaSeException(String message) {
        super(message);
    }
}
