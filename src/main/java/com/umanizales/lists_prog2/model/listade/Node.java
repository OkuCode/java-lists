package com.umanizales.lists_prog2.model.listade;

import com.umanizales.lists_prog2.model.Boy;
import lombok.Data;

@Data
public class Node {
    private Boy data;
    private Node next;
    private Node previous;

    /**
     * Constructor for the node class for the data structure called
     * Double linked list, it has a next pointer and previous pointer
     * When you don't create a node you have null, it is created you have
     * A node with a next pointer equal to null and previous equal null
     * @param data parameter that collect the data of the boy such as name
     * age, id, etc*/

    public Node(Boy data) {
        this.data = data;
    }
}