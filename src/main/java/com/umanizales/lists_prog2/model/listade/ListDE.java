package com.umanizales.lists_prog2.model.listade;

import com.umanizales.lists_prog2.controller.dto.*;
import com.umanizales.lists_prog2.exception.ListaDeException;
import com.umanizales.lists_prog2.model.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class ListDE {
    private Node head;
    private int count;

    /**
     * Method add a new instance of Boy class on the listDE
     * @param boy needed the data to append to the list */
    public void add(Boy boy){
        // Verify if the head has something
        if(this.head!=null)
        {
            // Create a new node called temp
            Node temp = this.head;
            while(temp.getNext()!=null)
            {
                temp = temp.getNext();
            }
            // Create a new node, temp pointer has this new node in the previous
            // Equal to temp
            Node newBoy= new Node(boy);
            temp.setNext(newBoy);
            newBoy.setPrevious(temp);
        }

        // If is empty add the new node
        else
        {
            this.head= new Node(boy);
        }
    }

    /** Method count
     * @return count the total of instance presents on the lists*/
    public int count(){
        int count = 0;
        if(this.head != null){
            Node temp = this.head;
            while (temp != null){
                temp = temp.getNext();
                count += 1;
            }
        }

        return count;

    }

    /**
     * Method findById
     * @param identification, this is the id you want to check if exists
     * @return null if the id doesn't exist*/
    public Boy findById(String identification){
        Node temp = this.head;
        while(temp != null){
            if(temp.getData().getIdentification().equals(identification)){
                return temp.getData();
            }
            temp = temp.getNext();
        }
        return null;
    }

    /**Method validateListEmpty check if the list has data available
     * @throws ListaDeException if there's no data available*/
    public void validateListEmpty() throws ListaDeException{
        if(this.head == null){
            throw new ListaDeException("No data available");
        }
    }

    /** Method listByGender this method show a list by gender
     * @param gender is the parameter to check when the new list is created
     * @return listGender the list of boys by gender*/
    public ListDE listDeBoysByGender(String gender) throws ListaDeException{
       validateListEmpty();
       Node temp = this.head;
       ListDE genderList = new ListDE();
       while (temp != null){
           if(temp.getData().getGender().name().equals(gender)){
               genderList.add(temp.getData());
           } temp = temp.getNext();
       } return genderList;
   }

    /** Method reorderByGender: The list become like: male, female, male, female
     *
     * @throws ListaDeException if the list doesn't have data available
     */
    public void reorderByGender() throws ListaDeException{
        validateListEmpty();
        ListDE boys = this.listDeBoysByGender("MASCULINO");
        ListDE girls = this.listDeBoysByGender("FEMENINO");

        ListDE minList = null;
        ListDE maxList = null;

        if(boys.getCount() > girls.getCount()){
            minList = girls;
            maxList = boys;
        } else{
            minList = boys;
            maxList = girls;
        }

        Node temp = minList.getHead();
        int index = 2;
        while(temp != null){
            maxList.addByIndex(temp.getData(), index);
            index += 2;
            temp = temp.getNext();
        } this.head = maxList.getHead();
    }

    /**Method addToStart insert to the beginning the instance of a boy class
     * @param boy needed to insert the beginning the instance of a boy class
     */
    public void addToStart(Boy boy){
        // Verify if the head is empty
        if(this.head == null){
            this.head = new Node(boy);
        } else {
            // Create the new node and set the pointer in the correct order
            Node newNode = new Node(boy);
            newNode.setNext(this.head);
            this.head.setPrevious(newNode);
            this.head = newNode;
        } count += 1;
    }

    /**Method add by index
     * @param index this is the position of a specific place of the selected node
     * @param boy this is the parameter that needs the method to insert the data**/
    public void addByIndex(Boy boy, int index){
        // If the position is greater than the count insert the child
        if(index > count){
            this.add(boy);
        } else if(index == 1){
            // If is the first position insert to begin
            addToStart(boy);
        } else {
            // If the position has two nodes in order
            int count = 1;
            Node temp = this.head;
            while(temp != null){
                if(count == index - 1){
                    // Create the new node
                    Node newNode = new Node(boy);
                    temp.setNext(newNode);
                    // New node take the next node from the temp
                    newNode.setNext(temp.getNext());
                    newNode.setPrevious(temp);
                    break;
                }
                temp = temp.getNext();
                count += 1;
            }
        }

    }

    /**Method inverts
     * This allows to present the list in reverse order
     */
    public void invert() {
        if (this.head != null) {
            ListDE listTemp = new ListDE();
            Node temp = this.head;
            while (temp != null) {
                listTemp.addToStart(temp.getData());
                temp = temp.getNext();
            }
            this.head = listTemp.getHead();
        }
    }

    /**Method deleteById
     * @param identification need to specify the correct id for the child**/
    public void deleteById(String identification){
        // Call a temp node
        Node temp = this.head;
        // Loop Through the list
        while (temp != null){
            temp = temp.getNext();
            // Verify if the id is equals to the parameter
            if (temp.getData().getIdentification().equals(identification) && temp == this.head){
                // Check if temp in next pointer is null
                if(temp.getNext() == null){
                    temp = null;
                    this.head = null;
                    return;
                } else {
                    // When the list has nodes, but you want to delete the first
                    // Store the next node to a container
                    Node container = temp.getNext();
                    // The previous to the container will be null because is the first
                    container.setPrevious(null);
                    temp = null;
                    // The head take the new position
                    this.head = container;
                }
            } else if(temp.getData().getIdentification().equals(identification)){
                if(temp.getNext() != null){
                    // When you want to delete the node to the middle
                    // Create a node container that stores the next node of temp
                    Node container = temp.getNext();
                    // Create a node prev that stores the prev node of temp
                    Node prev = temp.getPrevious();
                    // Set the pointer previous of the container to the prev node
                    container.setPrevious(prev);
                    // Set the next pointer of the prev container to the container
                    prev.setNext(container);
                    // Temp is not useful anymore, so it becomes null
                    temp = null;
                    return;
                } else {
                    // When is the last node to delete
                    // Create a prev container to store the previous node of temp
                    Node prev = temp.getPrevious();
                    // How the next pointer of prev container is null, so is set to null
                    prev.setNext(null);
                    temp = null;
                    return;
                }
            }
        }

    }

    /**Method total boys by locations
     * @param code: this code check if the data exists
     * @return count: total of children on the list with the specific code*/
    public int totalBoysByLocation(String code){
        Node temp = this.getHead();
        int count = 0;
        while(temp != null){
            if(temp.getData().getLocation().getCode().equals(code)){
               count += 1;
            } temp = temp.getNext();
        }
        return count;
    }

    /**Method print Boys: this method show the double linked list
      @throws ListaDeException if there's no data available*/
    public List<Boy> printListBoys() throws ListaDeException{
        if(this.head != null){
            Node temp = this.head;
            List<Boy> list = new ArrayList<>();
            while(temp != null){
                list.add(temp.getData());
                temp = temp.getNext();
            } return list;
        }
        throw new ListaDeException("No data available");
    }

    /** Method getGenderByGradeByLocation: this method allows to get the total of boys for grade and location,
     * also verify the total of orphans for their respective locations
     * @param grade: grade of the boys like 1st, 2nd, 3rd
     * @param location: location of where there are belonged: Manizales, Chinchina
     * @return GenderByGradesDTO: represents the object with all respective data for all the boys
     * @throws ListaDeException: if the list is empty throws an error**/
    public GenderByGradesDTO getGenderByGradeByLocation(byte grade, Location location) throws ListaDeException{
        // Validate if the list is empty
        validateListEmpty();
        // Here store the total of boys: total : Orphans, totalMale: boys with gender male, totalFemale: boys with
        // gender female
        int total= 0;
        int totalMale = 0;
        int totalFemale = 0;

        // Create a new temp and place it on the head
        Node temp = this.head;
        while(temp != null){
            // First verify if the location code is equals to the parameter
            if(temp.getData().getLocation().getCode().equals(location.getCode())
            && temp.getData().getGrade() == grade){
                total += 1;

                // Check if the boy is orphan and store the value on the respective counter
                if(temp.getData().isOrphan()){
                    if(temp.getData().getGender().equals(Gender.MASCULINO)){
                        totalMale += 1;
                    } else {
                        totalFemale += 1;
                    }
                }
            }
            temp = temp.getNext();
        }

        // We create a list, that store the following: Gender, Location, and the total of boys, also it is
        // Orphan or Not
        List<CountByGenderDTO> countByGenderDTOs = new ArrayList<>();
        countByGenderDTOs.add(new CountByGenderDTO(Gender.MASCULINO, totalMale));
        countByGenderDTOs.add(new CountByGenderDTO(Gender.FEMENINO, totalFemale));
        // Return the new Object with the grade, total of orphans
        return new GenderByGradesDTO(grade, total, countByGenderDTOs);
    }

    /**Method getGradesByLocation: this method allows to get all the boys in their respective grade
     * @param location: parameter that needs to get the specific location of the boy
     * @throws ListaDeException: thorws and exception if there's no data on the list
     * @return GradesByLocationDTO: and object with the respective location and also the data of the boy*/
    public GradesByLocationDTO getGradesByLocation(Location location) throws ListaDeException{
       List<GenderByGradesDTO> genderByGradesDTOList = new ArrayList<>();
       for (byte i = 1; i <=5; i++){
           genderByGradesDTOList.add(getGenderByGradeByLocation(i, location));
       }

        return new GradesByLocationDTO(location, genderByGradesDTOList);
    }


    /**Method gerGradesByRH this method allow to now the quantity of boys with RH, RH is the blood group here
     *
     * @param grade parameter need for grade, it's needed for know all the grades
     * @return GradeRHDTO an object with all the data grade, RH, and total
     * @throws ListaDeException if there's no data on the list
     */
    public GradeRHDTO getGradesByRH(byte grade) throws ListaDeException {
        // we check if the list is empty
        validateListEmpty();
        Node temp = this.head;
        int count = 0;
        String RHS = "";

        while(temp != null){
            //Check the grade of temp node
            if(temp.getData().getGrade() == grade){
                if(temp.getData().getRH().contains(temp.getData().getRH())){
                    // Here concatenate the RHS with the data of RH
                    RHS = RHS + "," + temp.getData().getRH();
                }
                // Total goes up by one
                count +=1;
            }

            temp = temp.getNext();
        }
        // Here return the new object with the grade, total and the concatenation
        return new GradeRHDTO(grade, count, RHS);
    }


    /**Method getGenderGradesByRHDTO this method call the grades 1 to five
     *@param gender is needed to separate the two gender Female and Male
     *@return GenderGradesByRHDTO and object with the gender and all the RHS
     *@throws ListaDeException if the list is empty*/
    public GenderGradesByRHDTO getGenderGradesByRHDTO(Gender gender) throws ListaDeException{
        // Create an array with the grades
        GradeRHDTO [] gradeRHDTOS = new GradeRHDTO[5];
        // Iterate the all grades
        for(int i = 0 ; i < 5; i++){
            // Here append the content on the list an increment the count by one
           gradeRHDTOS[i] = getGradesByRH((byte) i++);
        }
        // Return the new object with the gender and the grade
        return new GenderGradesByRHDTO(gender, gradeRHDTOS);

    }

    /**
     * Method getCountBoyByLocationByGenderByRH this method allows to append all the information
     * on a ArrayList in order to present all the data to the user
     * @param location: check if the location that has the object is the same
     * @return GenderGradesByLocationByRHDTO: the object with the location and all the information
     * @throws ListaDeException if there's no data on the list*/
    public GenderGradesByLocationByRHDTO getCountBoyByLocationByGenderByRH(Location location) throws ListaDeException{
        validateListEmpty();
        List<GenderGradesByRHDTO> genderGradesByRHDTOS = new ArrayList<>();
        genderGradesByRHDTOS.add(getGenderGradesByRHDTO(Gender.FEMENINO));
        genderGradesByRHDTOS.add(getGenderGradesByRHDTO(Gender.MASCULINO));
        return new GenderGradesByLocationByRHDTO(location, genderGradesByRHDTOS);

    }

}
