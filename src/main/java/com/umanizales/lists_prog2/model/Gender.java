package com.umanizales.lists_prog2.model;

/**
 * Enum to manipulate the gender of all boys
 * @author Juan David Rodas Barco
 */
public enum Gender {
    MASCULINO, FEMENINO;
}
