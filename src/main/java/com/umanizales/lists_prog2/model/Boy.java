package com.umanizales.lists_prog2.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

/**
 * Class for the boy object, this allows to manipulate
 * all the data from the boy object
 * @autor JD Rodas Barco
 * @version 1.0 05-11-2021
 */

@Data
@AllArgsConstructor
public class Boy {
    @NotNull
    @NotEmpty
    @Size(min=2)
    private String identification;
    @NotNull
    @NotEmpty
    @Size(min=2, max = 50)
    private String name;
    @Positive
    private byte age;
    @NotNull
    private Gender gender;
    @Valid
    @NotNull
    private Location location;
    @NotNull
    @NotEmpty
    private byte grade;
    @NotNull
    private boolean isOrphan;
    @NotNull
    private String RH;
}
