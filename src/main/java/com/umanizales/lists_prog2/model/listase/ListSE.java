package com.umanizales.lists_prog2.model.listase;

import com.umanizales.lists_prog2.exception.ListaSeException;
import com.umanizales.lists_prog2.model.Boy;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ListSE {
    /**
     * Only has two attributes the head
     * and a count variable to manage some operation like delete
     * for example
     */

    private Node head;
    private int count;

    /**
     * Method add to the end of the list
     * @param boy this is the parameter for the method add, it's from the Boy class
     * @throws ListaSeException if the identification of the boy is present to the list
     */
    public void add(Boy boy) throws ListaSeException {
        // We need first verify if the data is not the same as the data
        // will be added to the linked list

        Boy boyExist= findByIdentification(boy.getIdentification());
        if(boyExist !=null)
        {
            // If exists throws and exception

            throw  new ListaSeException("The id exists");
        }

        // Here we need to verify if the head of the list is null
        if(head == null)
        {
            // Head it's equal to new node
            head = new Node(boy);
        }
        else
        {
            // We need create a temp and loop through the linked list
            Node temp = head;
            while(temp.getNext()!=null)
            {
                temp = temp.getNext();
            }
            // The last position will have the node in temp.next*/
            temp.setNext(new Node(boy));
        }
        // Total of instance of a boy
        count++;
    }

    /**
     * Method adds to start
     * Only prepend the new instance to begin
     * @param boy this is the parameter for the method addToStart, it's from the Boy class
     * @throws ListaSeException if the identification of the boy is present to the list
     */
    public void addToStart(Boy boy) throws ListaSeException {
        // Here we need to verify if the boy identification is the same as the new instance
        Boy boyExist= findByIdentification(boy.getIdentification());

        // Verify if the id is the same as the instance presents in to the linked list
        if(boyExist !=null)
        {
            throw  new ListaSeException("The give identification doesn't exists");
        }

        if( this.head==null)
        {
            this.head = new Node(boy);
        }

        // Loop throughout the linked list and the new instance
        // to the next and the head is equal to the temporal node

        else
        {
            Node temp = new Node(boy);
            temp.setNext(this.head);
            this.head= temp;
        }
        count++;
    }

    /**
     * Method add by position
     * @param boy this is the parameter for the method addByPositioin, it's from the Boy class
     * @param position this is a parameter that you need if you want to know the exact position on a node
     *                 on the list
     * @throws ListaSeException if the indentification of the boy is present to the list*/
    public void addByPosition(Boy boy, int position) throws ListaSeException {
        // Here we need to verify if the boy identification is the same as the new instance
        Boy boyExist= findByIdentification(boy.getIdentification());
        if(boyExist !=null)
        {
            throw  new ListaSeException("The identification exists");
        }

        // Verify if the position is greater than the count
        if(position > count)
        {
            // Call the method add
            this.add(boy);
            return;
        }

        // If the position is the first than add the instance to the beginning
       else if(position == 1)
        {
            addToStart(boy);
        }
        else
        {
            // Begin the counter on 1
            int cont=1;
            Node temp = this.head;
            while(temp!=null)
            {
                if(cont == position - 1)
                {
                    break;
                }
                temp= temp.getNext();
                cont++;
            }
            // The temporal will be in the previous position
            Node nodeNew= new Node(boy);
            nodeNew.setNext(temp.getNext());
            temp.setNext(nodeNew);
            count++;
        }
    }

    /**
     * Method to present the list in invert order*/
    public void invert() throws ListaSeException{
        // First we need to verify if the head have something
        if (this.head != null) {
            // Here store the linked list on another list called listTemp
            ListSE listTemp = new ListSE();
            Node temp = this.head;
            // Loop through it
            while(temp != null)
            {
                listTemp.addToStart(temp.getData());
                temp = temp.getNext();

            }
            // The head will be the last node of the linked list
            this.head = listTemp.getHead();
        }
    }

    /**Method count throw the total of nodes presents inside a linked list
     * @return count total of nodes in the list
     */
    public int count() {
        int count = 0;
        if (this.head != null) {
            Node temp = this.head;
            while (temp != null)
            {
                count++;
                temp = temp.getNext();
            }
        }
        return count;
    }

    /**
     * Method ListPresent the list of nodes
     * @throws ListaSeException if there's no data on the list
     * @return list, shows the list of nodes**/
    public List<Boy> list() throws ListaSeException {
        // Verify if the head have something**/
        if(this.head!=null)
        {
            // This store the data on list of boys
            Node temp = this.head;
            List<Boy> list= new ArrayList<>();
            while (temp != null)
            {
                list.add(temp.getData());
                temp = temp.getNext();
            }
            return list;
        }

        throw  new ListaSeException("No data on the list");
    }

    /**
     * Change extremes
     * @throws ListaSeException if there's at least two nodes*/
    public void changeXtremes() throws  ListaSeException{
        // We need verify to cases
        // 1) if head has something
        // 2) if head in next pointer has something
        if (this.head != null && this.head.getNext() != null) {
            // Creating a new boy with the data of head
            Boy start = this.head.getData();
            Node temp = head;
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            this.head.setData(temp.getData());
            temp.setData(start);

        }
        else
        {
            throw  new ListaSeException("Invalid Operation, cannot change the order");
        }
    }

    /**
     * Method delete by identification
     * @param identification is the parameter to check on our method
     * @throws ListaSeException 1) if the id is not presented on the list
     *                          2) if there's no data on the list*/
    public void delete(String identification) throws ListaSeException {
        // Verify if the head has something
        if(this.head!=null)
        {
            if(this.head.getData().getIdentification().equals(identification))
            {
                this.head = this.head.getNext();
            }
            else
            {
                Node temp = this.head;
                while(temp!=null)
                {
                    if(temp.getNext() != null &&
                            temp.getNext().getData().getIdentification().equals(identification))
                    {
                        break;
                    }
                    temp= temp.getNext();
                }
                // Temp will be in the before node after delete it
                if(temp!= null)
                {
                    temp.setNext(temp.getNext().getNext());
                }
                else
                {
                    throw  new ListaSeException("Identification: "+identification + " doesn't exist");
                }
            }
        }
        else
        {
            throw  new ListaSeException("No data on the list");
        }
    }

    /**
     * Method that find on the list a boy with the identification
     * If don't find it, it will be null
     * @param identification id, TI, CE, Sisben or something that allow to find the id
     * @return found boy
     */
    public Boy findByIdentification(String identification) {
        // Head can't move, so you need to call a temp for all this task
        Node temp= this.head;
        // Through the linked list, when the temp reach to the end my temp will be null
        while(temp!=null)
        {
            // Ask if the id is equal to the id of the boy
            if(temp.getData().getIdentification().equals(identification))
            {
                // if is equal retrieve the data

                return temp.getData();
            }
            // temp keep loop

            temp=temp.getNext();
        }
        // if not equal, there no data to display

       return null;
    }

    /** Method validate list empty
     * @throws ListaSeException if there's no data on the list*/
    public void validateListEmpty() throws ListaSeException {
        // Verify if the head is null
        if(this.head==null)
        {
            throw new ListaSeException("There's no data on the list");
        }
    }

    /**Method get boys by gender
     * @param gender if the parameter gender to locate the boy on the list with the desire gender
     * @throws ListaSeException if the list is empty
     * @return listTemp is the list with the genders of boys
     * */
    public ListSE getListSeBoysByGender(String gender) throws ListaSeException {
        // This validate if the list is empty
        validateListEmpty();
        Node temp= this.head;
        //Create a new list temp and store the original list
        ListSE listTemp = new ListSE();
        while(temp !=null)
        {
            if(temp.getData().getGender().name().equals(gender))
            {
                listTemp.add(temp.getData());
            }
            temp = temp.getNext();
        }
        return listTemp;
    }

    /**Method variant boys
     * @throws ListaSeException if the list is empty*/
    public void variantBoys() throws ListaSeException {
        // Validate if the list is empty
        validateListEmpty();
        // Create two list with the genders
        ListSE boys= this.getListSeBoysByGender("MASCULINO");
        ListSE girls= this.getListSeBoysByGender("FEMENINO");
        // Create a min and max list store the total of boys
        ListSE minList= null;
        ListSE maxList= null;

        // Check if the total of boys is greater than total of girls
        if(boys.getCount() > girls.getCount())
        {
            // Asking the quantity to the respective lists
            minList= girls;
            maxList = boys;
        }
        else
        {
            minList = boys;
            maxList = girls;
        }
        // Call a temporal and put in the head of the list with fewer children
        Node temp= minList.getHead();
        // Initialize a position variable on 2
        int pos=2;
        while(temp != null)
        {
            // add the children of the min list to the max list
            maxList.addByPosition(temp.getData(), pos);
            pos = pos +2;
            temp = temp.getNext();
        }
        // The head stay on max list
        this.head= maxList.getHead();

    }

    /** Method total of boys by location
     * @param code is the code for the respective location example: 19217001
     * @return count total of children by the respective location*/
    public int getCountBoysByLocation(String code) {
        Node temp= this.getHead();
            int count=0;
            while(temp != null)
            {
                if(temp.getData().getLocation().getCode().equals(code))
                {
                    count++;
                }
                temp = temp.getNext();
            }
            return count;

    }

}

