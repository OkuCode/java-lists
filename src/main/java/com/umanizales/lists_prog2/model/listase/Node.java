package com.umanizales.lists_prog2.model.listase;

import com.umanizales.lists_prog2.model.Boy;
import lombok.Data;

@Data
public class Node {
    private Boy data;
    private Node next;

    /**
     * Constructor for the node class, you need a Boy to create a new node
     * if you don't specify it, you get an empty node, and the pointer will be null
     * @param data parameter that collect the data of the boy such as age, name etc.
     */
    public Node(Boy data) {
        this.data = data;
    }
}
