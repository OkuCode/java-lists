package com.umanizales.lists_prog2.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Implemented class to manage countries, cities and states
 * Example: code:169 description:Colombia
 * Example 2: code:16917 description:Caldas
 * Example 3: code:16917001 description: Manizales
 * @author Juan David Rodas Barco
 */

@Data
@AllArgsConstructor
public class Location {
    @NotNull
    @NotEmpty
    private String code;
    @NotNull
    @NotEmpty
    private String description;
}
