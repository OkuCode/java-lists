package com.umanizales.lists_prog2.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Class boys by location: this class is need to ensure
 * The instance of the boy needs the location arguments
 * Like code, and description
 * @author Juan David Rodas Barco
 */

@Data
@AllArgsConstructor
public class BoysByLocation {
    private Location location;
    private int count;
}
