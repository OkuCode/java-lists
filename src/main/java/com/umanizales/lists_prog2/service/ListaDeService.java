package com.umanizales.lists_prog2.service;

import com.umanizales.lists_prog2.controller.dto.GradesByLocationDTO;
import com.umanizales.lists_prog2.controller.dto.ResponseDTO;
import com.umanizales.lists_prog2.exception.ListaDeException;
import com.umanizales.lists_prog2.model.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.umanizales.lists_prog2.model.listade.ListDE;

import java.util.ArrayList;
import java.util.List;


/***
 * Class ListaDeService: control all the behaviours of our app
 * And also allows to call all the methods presents on the class
 * ListDE, the decorator that brings springboot change this class in
 * order to become a service for our SpringBoot app
 * @autor Juan David Rodas Barco*/

@Service
public class ListaDeService {
    private ListDE listBoys;
    private List<Location> locations;

    public ListaDeService(){
        listBoys = new ListDE();
        initLocations();
    }

    public void initLocations(){
        locations = new ArrayList<>();
        locations.add(new Location("16917001", "Manizales"));
        locations.add(new Location("16917003", "Chinchina"));
    }

    public boolean validateLocation(Location location){
        for(Location loc: locations){
           if(loc.getCode().equals(location.getCode()) && loc.getDescription().equals(location.getDescription())){
               return true;
           }
        } return false;
    }

    public ResponseEntity<ResponseDTO> addBoy(Boy boy) throws ListaDeException{
        if(!validateLocation(boy.getLocation())){
            throw new ListaDeException("The location is invalid");
        }
        listBoys.add(boy);
        return new ResponseEntity<>(
                new ResponseDTO("Successfully added", true, null),
                HttpStatus.OK);

    }

    public ResponseEntity<ResponseDTO> Count(){
        return new ResponseEntity<>(
                new ResponseDTO("Success", listBoys.count(), null),
                HttpStatus.OK);
    }

    public ResponseEntity<ResponseDTO> addBoyToStart(Boy boy){
        listBoys.addToStart(boy);
        return new ResponseEntity<>(
                new ResponseDTO("Successfully added", true, null),
                HttpStatus.OK);
    }

    public ResponseEntity<ResponseDTO> addByIndex(Boy boy, int index){
        listBoys.addByIndex(boy, index);
        return new ResponseEntity<>(
                new ResponseDTO("Successfully added", true, null),
                HttpStatus.OK);
    }

    public ResponseEntity<ResponseDTO> invert(){
        listBoys.invert();
        return new ResponseEntity<>(
                new ResponseDTO("Success", listBoys.getHead(), null),
                HttpStatus.OK
        );
    }

    public ResponseEntity<ResponseDTO> deleteById(String identification){
        listBoys.deleteById(identification);
        return new ResponseEntity<>(
                new ResponseDTO( "Success", true, null),
                HttpStatus.OK
        );
    }

    public ResponseEntity<ResponseDTO> listBoysByGender(String gender) throws ListaDeException {
        listBoys.listDeBoysByGender(gender);
        return new ResponseEntity<>(
                new ResponseDTO("Success", listBoys.listDeBoysByGender(gender), null),
                HttpStatus.OK);
    }

    public ResponseEntity<ResponseDTO> reorderByGender() throws ListaDeException{
        listBoys.reorderByGender();
        return new ResponseEntity<>(
                new ResponseDTO("Success", listBoys.getHead(), null),
                HttpStatus.OK);
    }

    public ResponseEntity<ResponseDTO> totalBoysByLocation(){
        List<BoysByLocation> boysLocations = new ArrayList<>();
        for (Location loc: locations){
            int count = listBoys.totalBoysByLocation(loc.getCode());
            boysLocations.add(new BoysByLocation(loc, count));
        } return new ResponseEntity<>(
                new ResponseDTO("Success", boysLocations, null),
                HttpStatus.OK);
    }

    public ResponseEntity<ResponseDTO> listBoysDe() throws ListaDeException{
        if(listBoys.getHead() == null){
            throw new ListaDeException("There's no data available");
        } return new ResponseEntity<>(
                new ResponseDTO("Success", listBoys.getHead(), null),
                HttpStatus.OK);
    }

    public ResponseEntity<ResponseDTO> getOrphansByGradesByLocation(byte grade, Location location) throws ListaDeException {
        if(listBoys.getHead() == null){
            throw new ListaDeException("There's no data available");
        }
        List<GradesByLocationDTO> gradesByLocationDTOS = new ArrayList<>();
        return null;
    }

    public ResponseEntity<ResponseDTO> getGradesByLocation() throws ListaDeException{
        if (listBoys.getHead() == null){
            throw new ListaDeException("There's no data available");
        }
        return new ResponseEntity<>(
                new ResponseDTO("Success" , true, null), HttpStatus.OK);

    }
/*
    public ResponseEntity<ResponseDTO> totalBoysByLocation(){
        List<BoysByLocation> boysLocations = new ArrayList<>();
        for (Location loc: locations){
            int count = listBoys.totalBoysByLocation(loc.getCode());
            boysLocations.add(new BoysByLocation(loc, count));
        } return new ResponseEntity<>(
                new ResponseDTO("Success", boysLocations, null),
                HttpStatus.OK);
    }
*/

}
